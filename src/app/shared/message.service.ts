import {Subject} from "rxjs";
import {Message} from "./message.model";
import {HttpClient, HttpParams} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {Injectable} from "@angular/core";

@Injectable()
export class MessageService {
  messagesChange = new Subject<Message[]>();
  messageFetching = new Subject<boolean>();
  messageUploading = new Subject<boolean>();
  messageSendingStop = new Subject<boolean>();

  constructor(private http: HttpClient) {
  };

  private messages: Message[] = [];

  stop() {
    this.messageFetching.next(true);
    this.http.get<{ [id: string]: Message }>('http://146.185.154.90:8000/messages')
      .pipe(map(result => {
        return Object.keys(result).map(_id => {
          const post = result[_id];
          console.log(post);
          return new Message(
            post.author,
            post.datetime,
            post.message,
            post._id,
          );
        });
      }))
      .subscribe(messages => {
        this.messages = messages;
        this.messagesChange.next(this.messages.slice());
        this.messageFetching.next(false);
      }, () => {
        this.messageFetching.next(false);
      })
  }




  start(post: Message) {
    const body = new HttpParams()
    .set('author', post.author)
    .set('message', post.message);
    this.messageUploading.next(true);
    return this.http.post('http://146.185.154.90:8000/messages', body).pipe(
      tap(() => {
        this.messageUploading.next(false);
      }, () => {
        this.messageUploading.next(false);
      })
    )
  }
}

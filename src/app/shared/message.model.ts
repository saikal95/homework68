
export class Message {
  constructor(
    public author: string,
    public datetime: string,
    public message: string,
    public _id: string,
  ) {
  }
}

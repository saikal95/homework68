import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {Subscription} from "rxjs";
import {Message} from "../shared/message.model";
import {MessageService} from "../shared/message.service";


@Component({
  selector: 'app-one-message',
  templateUrl: './one-message.component.html',
  styleUrls: ['./one-message.component.css']
})
export class OneMessageComponent implements OnInit, OnDestroy{
  @Input() message!: Message;

  isRemoving = false;
  messageRemovingSubscription!: Subscription;


  constructor(private route: ActivatedRoute,
              private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.messageRemovingSubscription = this.messageService.messageSendingStop.subscribe((isRemoving:boolean) => {
      this.isRemoving = isRemoving;
    })
  }

  ngOnDestroy() {
    this.messageRemovingSubscription.unsubscribe()
  }



}




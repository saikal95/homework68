import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MessageService} from "../shared/message.service";
import {Message} from "../shared/message.model";
import {Subscription} from "rxjs";
import {NgForm} from "@angular/forms";


@Component({
  selector: 'app-message-board',
  templateUrl: './message-board.component.html',
  styleUrls: ['./message-board.component.css']
})
export class MessageBoardComponent implements OnInit, OnDestroy {
  @ViewChild('f') messageForm!: NgForm;
  messages!: Message[];
  message!: Message;
  loading = false;
  loadingSec = false;

  messageChangeSubscription!: Subscription;
  messageFetchingSubscription!: Subscription;
  messageLoading!: Subscription;

  constructor(private messageService: MessageService) {
  }

  ngOnInit() {
    this.messageChangeSubscription = this.messageService.messagesChange.subscribe((messages: Message[]) => {
      this.messages = messages;

      this.messageLoading = this.messageService.messageUploading.subscribe((loading: boolean) => {
        this.loading = loading;
      })
    })
    this.messageFetchingSubscription = this.messageService.messageFetching.subscribe((isFetching: boolean) => {
      this.loadingSec = isFetching;
    })
  }


  onSend() {
    const message = new Message(this.messageForm.value.author, '', this.messageForm.value.message, '')
    this.messageService.start(message).subscribe();
  }


  onStart() {
    this.messageService.stop()
  }

  ngOnDestroy() {
    this.messageLoading.unsubscribe();
  }

}
